import { stringifyUrl } from "query-string"
import { setContext } from "svelte"
import { writable } from "svelte/store"
import { browser } from "$app/env"
import { page } from "$app/stores"
import * as storage from "$lib/storage"
import { AUTH_CONTEXT_KEY } from "$lib/constants"

export function useAuth({ authRoutes, onAuthLoad, pkceConfig }) {
    function ensureAuthenticated(page) {
        if (browser && tokensFromStorage !== undefined) {
            if (tokensFromStorage === null) {
                window.location.assign(buildLoginPath(page))
            }
        }
    }

    function buildLoginPath(page) {
        return stringifyUrl({
            url: authRoutes.login, // TODO handle base path
            query: { returnTo: buildReturnTo(page) },
        })
    }

    function buildLogoutPath(page) {
        return stringifyUrl({
            url: authRoutes.logout, // TODO handle base path
            query: { returnTo: buildReturnTo(page) },
        })
    }

    function buildReturnTo(page) {
        return stringifyUrl({
            // Avoid auth routes being returnTo URLs.
            url: Object.values(authRoutes).includes(page.path)
                ? "/" // TODO Handle base path
                : page.path,
            query: Object.fromEntries(page.query.entries()),
        })
    }

    let tokensFromStorage = undefined
    const tokens = writable(undefined)

    if (browser) {
        setContext(AUTH_CONTEXT_KEY, {
            buildLoginPath,
            buildLogoutPath,
            ensureAuthenticated,
            page,
            pkceConfig,
            tokens,
        })
        if (!Object.values(authRoutes).includes(page.path)) {
            tokensFromStorage = storage.getTokens()
            tokens.set(tokensFromStorage)
            onAuthLoad(tokensFromStorage)
        }
    }

    return [buildLoginPath, buildLogoutPath]
}

