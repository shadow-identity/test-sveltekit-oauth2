// TODO replace with an existing storage abstraction

export function getReturnTo(): string | null {
    return getItem("returnTo")
}

export function setReturnTo(url: string): void {
    return setItem("returnTo", url)
}

export function getTokens(): Record<string, unknown> | null {
    return getJsonItem("tokens") as Record<string, unknown> | null
}

export function setTokens(tokens: Record<string, unknown>): void {
    return setJsonItem("tokens", tokens)
}

export function removeTokens(): void {
    return removeItem("tokens")
}

function getStorage() {
    return window.sessionStorage
}

function addPrefix(key: string): string {
    const PREFIX = "sveltekit-oauth2-pkce"
    return `${PREFIX}.${key}`
}

function getItem(key: string): string | null {
    return getStorage().getItem(addPrefix(key))
}

function setItem(key: string, data: string): void {
    return getStorage().setItem(addPrefix(key), data)
}

function removeItem(key: string): void {
    getStorage().removeItem(addPrefix(key))
}

function getJsonItem(key: string): unknown {
    return JSON.parse(getItem(key))
}

function setJsonItem(key: string, data: unknown): void {
    return setItem(key, JSON.stringify(data))
}
