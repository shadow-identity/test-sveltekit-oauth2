export async function createGitlabClient(session, tokens) {
	const { Gitlab } = (await import("@gitbeaker/browser")).default
	return new Gitlab({ host: session.config.gitlabUrl, oauthToken: tokens.access_token })
}

export async function fetchUserProfile(gitlabClient, userId) {
	return gitlabClient.Users.show(userId)
}

export async function fetchUserProjects(gitlabClient, userId) {
	return gitlabClient.Users.projects(userId)
}
