export interface Config {
	gitlabOauthClientId: string
	gitlabUrl: string
}

export function loadConfigFromEnv(): Config {
	// TODO parse and validate .env
	return {
		gitlabOauthClientId: import.meta.env.VITE_GITLAB_OAUTH_CLIENT_ID,
		gitlabUrl: import.meta.env.VITE_GITLAB_URL || "https://gitlab.com",
	}
}
