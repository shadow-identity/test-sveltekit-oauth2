import type { Config } from "./config"
import { loadConfigFromEnv } from "./config"

interface MyContext {
	config: Config
}

interface MySession {
	config: Config
}

export function getContext(): MyContext {
	const config = loadConfigFromEnv()
	return { config }
}

export function getSession({ context }: { context: MyContext }): MySession {
	// Do not return secrets here! Returned value is accessed from client via $session store.
	const { config } = context
	return { config }
}
