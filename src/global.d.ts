/// <reference types="@sveltejs/kit" />
/// <reference types="svelte" />
/// <reference types="vite/client" />

interface ImportMetaEnv {
	VITE_GITLAB_URL: string
	VITE_GITLAB_OAUTH_CLIENT_ID: string
}
